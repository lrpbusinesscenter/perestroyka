[background=white][box=black][center][size=10][b][color=black]PERESTROYKA ORGANIZATSIYA[/color][/b][/size][/center][/box][/background]
[background=white][box=black][right][i][color=black]Article de Laura King paru le 05 avril 2020*[/color]    [/i][/right][center][img]https://imgur.com/gbRsDBV.png[/img][/center][/box][/background]


[background=white][box=black][right][i][color=black]Rapport détaillé sur le cybercrime à Los Santos en 2019*[/color]    [/i][/right][center][img]https://www.hebergeur-image.com/upload/31.38.159.30-5e8e1b90a6c7f.jpg[/img][/center][/box][/background][background=white][box=black][center][url=https://drive.google.com/file/d/1Sou2RE2LCTtRpMEy38cRCIduCTbSpOLc/view?usp=sharing][img]https://imgur.com/7EiV1RT.jpg[/img][/url][/center]
[center][url=https://drive.google.com/file/d/1Sou2RE2LCTtRpMEy38cRCIduCTbSpOLc/view?usp=sharing]Cliquez pour visualiser notre étude[/url][/center][/box][/background]


[background=white][box=black][size=15][b][center]OOC[/center][/b][/size]

[justify][b]Qui sommes-nous ? [/b]

Nous sommes les anciens membres et leaders de la faction « The Russian Bratva organatsiya » qui a perduré cinq ans sur CMLV. Vous avez certainement déjà entendu parler des « vory v zakone », de la « bratva » ou encore du « code des voleurs », tous ces termes sont assez historiques à vrai dire et cache une culture qui est installée depuis plusieurs décennies en Russie, en France et pour ce qui nous concerne, aux États-Unis. La mafia russe installée aux États-Unis, que ce soit à New-York, dans le New-Jersey ou à Los Angeles, a connu plusieurs générations et on parle aujourd’hui de « russo-américains », des originaires russes nés sur le sol américain. Nous avons décidé de changer de nom dans un premier temps et d'incarner un groupe criminel local qui évoluera en un système tentaculaire qui contrôlera tout les groupes criminels régionaux originaire de l'ex bloc soviétique. Tout les joueurs sont acceptés parmi-nous, que vous soyez débutant ou confirmé, du moment que vous veniez pour apporter de la valeur ajoutée a la faction.

[b]La cybercriminalité[/b]

Il est évident que la cybercriminalité a pris beaucoup d’ampleur au fur et à mesure du temps et s’est installé au sein des organisations criminelles, malheureusement on retrouve rarement cet aspect sur le serveur et ce malgré la multitude de possibilités. Nous souhaitons mettre en place un réseau de cybercriminalité qui pourra évoluer sur le serveur grâce à la communauté et qui ne sera pas uniquement un réseau centré sur notre faction, je ne souhaite pas vous dévoiler nos objectifs à travers la partie OOC et vous laisser le plaisir de découvrir le tout par vous-même mais sachez que nous avons plusieurs développeurs qui travaillent sur un projet que l’on aimerait intégrer par la suite sur le serveur. La cybercriminalité sera le fil rouge de notre projet, toujours présente en arrière plan elle ne constituera pas la majorité de nos activités In-game mais elle apportera un plus non négligeable.

[b]Nous rejoindre[/b]

Si vous êtes décidé à nous rejoindre, veuillez PM Jorgio ou Bokassa peu importe la forme signalez simplement que vous nous rejoigniez.
Nous vous envoyons rapidement un lien vers le Discord avec tout le support pour réussir votre parcours chez nous.[/justify]

[i][right]
да пребудет с вами сансара![/right][/i][/box][/background]